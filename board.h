#ifndef __BOARD_H__
#define __BOARD_H__

#include <bitset>
#include "common.h"
using namespace std;

struct Point {
    int x, y;
    Point(int x, int y){
        this->x = x;
        this->y = y;
    }
};
class Board {
   
private:
    bitset<64> black;
    bitset<64> taken;    
       
    bool occupied(int x, int y);
    bool get(Side side, int x, int y);
    void set(Side side, int x, int y);
    bool onBoard(int x, int y);
      
public:
    Board();
    ~Board();
    Board *copy();
        
    bool isDone();
    bool hasMoves(Side side);
    bool checkMove(Move *m, Side side);
    void doMove(Move *m, Side side);
    int count(Side side);
    int countBlack();
    int countWhite();
    int findScore(Move *m, Board *b, Side side, bool testing, int turn);
	std::vector<Point*> AvailableMoves(Board * b, Side side);
    void setBoard(char data[]);
};
struct Node {
	Board * board;
	std::vector<Node*> children;
	int score;
	Move * move = new Move(0, 0);
	Node * parent;
	Node() {
		this->parent = NULL;
	}
	~Node() {
		delete move;
	}
};


#endif
