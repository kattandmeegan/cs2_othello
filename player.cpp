#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
    mySide = side;
    opSide = (mySide == WHITE) ? BLACK : WHITE;
    board = new Board();
    temp = new Move(0,0);
    turn = 0;

}

/*
 * Destructor for the player.
 */
Player::~Player() {
    delete board;
    delete temp;
}

void Player::testmode() {
    testingMinimax = true;
    char boardData[64] = {
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 
        ' ', 'b', ' ', ' ', ' ', ' ', ' ', ' ', 
        'b', 'w', 'b', 'b', 'b', 'b', ' ', ' ', 
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '
    };
    board->setBoard(boardData);
}


/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 

    //int score = 0;
    int x,y = 0;
    Node *best = new Node();
    if (opponentsMove != NULL)
    {
        // record where opponent moved
        board->doMove(opponentsMove, opSide);
    }
    
    if (!board->hasMoves(mySide))
    {
        return NULL;
    }
    else
    {
		if(testingMinimax == true)
		{
        // MINIMAX CRAP
        // sets up nodey tree thing
        Node *start = new Node();
        start->board = board;
        start->parent = NULL;
        std::vector<Point*> moves = board->AvailableMoves(board, mySide);
        std::vector<Point*>::iterator i;
//        Node *tnode = new Node();
//        Board *tboard = new Board();
//        Node *opNode = new Node();
//        Board *opBoard = new Board();
        for (i=moves.begin(); i!=moves.end(); i++)
        {
            Point *tPt = *i;
            Node *tnode = new Node();
            std::cerr << "Checking self move: " << tPt->x << ", " << tPt->y << '\n';
            //Node *tnode = new Node();
            tnode->parent = start;
            tnode->move = new Move(tPt->x, tPt->y);
            //Board *tboard = new Board();
            tnode->board = board->copy();
            tnode->board->doMove(tnode->move, mySide);
            tnode->score = 9001;
            //std::cerr << "x, y = " << tnode->move->getX() << ", " << tnode->move->getY() << '\n';
            std::vector<Point*> opMoves = tnode->board->AvailableMoves(tnode->board, opSide);
            std::vector<Point*>::iterator j;
            for (j=opMoves.begin(); j!=opMoves.end(); j++)
            {
                Point *opPt = *j;
                std::cerr << "\tChecking op move:   " << opPt->x << ", " << opPt->y << '\n';
                Node *opNode = new Node();
                opNode->parent = tnode;
                opNode->move = new Move(opPt->x, opPt->y);
                Board *opBoard = tnode->board->copy();
                opBoard->doMove(opNode->move, opSide);
                opNode->board = opBoard;
                opNode->score = opBoard->findScore(opNode->move, opBoard, opSide, testingMinimax, turn+1);
                std::cerr << "\t\tscore: " << opNode->score << '\n';
                tnode->children.push_back(opNode);
                //std::cerr << "x, y = " << opNode->move->getX() << ", " << opNode->move->getY() << '\n';
                /*if (opNode->score < tnode->score)
                {
                    tnode->score = opNode->score;
                }*/
                std::vector<Point*> d3moves = opNode->board->AvailableMoves(opNode->board, mySide);
                std::vector<Point*>::iterator k;
                for (k=d3moves.begin(); k!=d3moves.end(); k++)
                {
                    Point *d3pt = *k;
                    Node *d3node = new Node();
                    d3node->parent = opNode;
                    d3node->move = new Move(d3pt->x, d3pt->y);
                    Board *d3board = opNode->board->copy();
                    d3board->doMove(d3node->move, mySide);
                    d3node->board = d3board;
                    d3node->score = d3board->findScore(d3node->move, d3board, mySide, testingMinimax, turn+2);
                    opNode->children.push_back(d3node);
                    std::vector<Point*> d4Moves = d3node->board->AvailableMoves(d3node->board, opSide);
                    std::vector<Point*>::iterator k;
                    for (k=d3moves.begin(); k!=d3moves.end(); k++)
                    {
                        Point *d4pt = *k;
                        Node *d4node = new Node();
                        d4node->parent = d3node;
                        d4node->move = new Move(d4pt->x, d4pt->y);
                        Board *d4board = d3node->board->copy();
                        d4board->doMove(d4node->move, opSide);
                        d4node->board = d4board;
                        d4node->score = d4board->findScore(d4node->move, d4board, opSide, testingMinimax, turn+3);
                        opNode->children.push_back(d4node);
                        delete d4node;
                    }
                    delete d3node;
                }
                delete opNode;

            }
            start->children.push_back(tnode);
            //delete tnode;
        }
        // get highest min
        std::cerr << "-------\n";
        std::vector<Node*>::iterator a;
        int score = -9001;
        // for all tnodes
        for (a=start->children.begin(); a!=start->children.end(); a++)
        {
            Node *A = *a;
            std::vector<Node*>::iterator b;
            // for all opNodes
            for (b=A->children.begin(); b!=A->children.end(); b++)
            {
                Node *B = *b;
                std::vector<Node*>::iterator c;
                // for all d3nodes
                for (c=B->children.begin(); c!=B->children.end(); c++)
                {
                    Node *C = *c;
                    std::vector<Node*>::iterator d;
                    // for all d4nodes
                    for (d=C->children.begin(); d!=C->children.end(); d++)
                    {
                        
                        Node *checkNode = *d;
                        if (checkNode->score > score) {
                            std::cerr << "new min score: " << checkNode->score << '\n';
                            best = checkNode;
                            score = checkNode->score;
                        }
                    }
                }
            }
        }

        x = best->move->getX();
        y = best->move->getY();
	    }
		else
		{
        // HEURISTIC CRAP
        // scan board for possible moves
        int score = -100000;
        for (int i=0; i<8; i++)
        {
            temp->setX(i);
            for (int j=0; j<8; j++)
            {
                temp->setY(j);
                if (board->checkMove(temp, mySide))
                {
                    int new_score = board->findScore(temp, board, mySide, testingMinimax, 0);
					if(new_score > score || score == 0)
					{
						score = new_score;
                        x = temp->getX();
                        y = temp->getY();
					}
                }
            }
        }
		}
        
    }
    // weighting changes dynamically! wow!
    turn++;
    Move * finalMove = new Move(x, y); 
	board->doMove(finalMove, mySide);
    return finalMove;
}
